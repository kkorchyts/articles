package com.kkorchyts.articles.bigo;

import java.util.Arrays;

public class BigO {

    // функция возвращает количество элементов массива
    public static int getArraySize(int[] array) {
        return array.length;
    }

    // функция возвращает сумму элементов массива
    public static int getArraySum(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    // Функция возвращает массив, в ячейке которого находится сумма
    // произведений элемента, находящегося в идентичной ячейке
    // (с теми же индексами)  исходного массива на все другие
    // элементы исходного массива
    public static int[] getArrayOfMultiplicationSum(int[] array) {
        int[] resultArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                resultArray[j] += array[i] * array[j];
            }
        }
        return resultArray;
    }

    // функция (модифицированная) возвращает массив, в ячейке которого
    // находится сумма произведений элемента, находящегося
    // в идентичной ячейке (с теми же индексами)  исходного массива
    // на все другие элементы исходного массива
    public static int[] getArrayOfMultiplicationSumModified(int[] array) {
        int[] resultArray = new int[array.length];
        int arraySum = getArraySum(array);
        for (int i = 0; i < array.length; i++) {
            resultArray[i] = arraySum * array[i];
        }
        return resultArray;
    }



    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,4};
        System.out.println(getArraySize(array));
        System.out.println(getArraySum(array));
        System.out.println(Arrays.toString(getArrayOfMultiplicationSum(array)));
        System.out.println(Arrays.toString(getArrayOfMultiplicationSumModified(array)));
    }
}
