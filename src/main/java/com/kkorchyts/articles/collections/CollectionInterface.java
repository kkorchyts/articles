package com.kkorchyts.articles.collections;

import javafx.print.Collation;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Spliterator;
import java.util.Stack;
import java.util.Vector;

public class CollectionInterface {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        Vector<Integer> vector = new Vector<>();
        Stack<Integer> stack = new Stack<>();
        LinkedList<Integer> linkedList = new LinkedList<>();

        int iteration = 0;
        Integer capacity = 10;
        Integer delta = 0;
        do {
            delta = capacity >> 1;
            System.out.println("Capacity = " + capacity + "; delta = " + delta);
            capacity += delta;
        } while (iteration++ < 10 );
        System.out.println('1'+ '1');
    }

    static class User {
        private String name;
        private Integer age = 0;


        public User(String name) {
            this.name = name;
        }

        public void grow(){
            age++;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }

    static class Iterable_Example {
        public static void main(String[] args) {
            List<User> list = Arrays.asList(new User("1"), new User("2"), new User("3"));
            list.forEach(user -> user.grow());
            ArrayList<User> users =  new ArrayList<>(Arrays.asList(new User("1"), new User("2"), new User("3")));
            Iterator<User> iterator = users.listIterator();
            for (User user: list) {
                System.out.println(user.age);
            }
            Spliterator<User> spliterator =  list.spliterator();
            System.out.println(spliterator);
        }

    }

    static class IteratorExample {
        public static void main(String[] args) {
            // List initialization
            List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 2));

            // Print initial list and remove elements equal 2 from it
            System.out.println("Initial list:");
            for (Iterator<Integer> i = list.iterator(); i.hasNext();){
                Integer currentInteger = i.next();
                System.out.println(currentInteger);
                if (currentInteger == 2) {
                    i.remove();
                }
            }

            // Print final list
            System.out.println("Final list:");
            for (Iterator<Integer> i = list.iterator(); i.hasNext();){
                Integer currentInteger = i.next();
                System.out.println(currentInteger);
            }
        }
    }

    static class IterableExample {
        public static void main(String[] args) {
            // List initialization
            List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 2));

            // Print initial list and remove elements equal 2 from it
            System.out.println("Initial list:");
            for (Iterator<Integer> i = list.iterator(); i.hasNext();){
                Integer currentInteger = i.next();
                System.out.println(currentInteger);
                if (currentInteger == 2) {
                    i.remove();
                }
            }

            // Print final list
            System.out.println("Final list:");
            for (Integer i : list){
                System.out.println(i);
            }

            Stack<Integer> stack = new Stack<>();
        }
    }

    static class ArrayListExample {
        public static void main(String[] args) {
            // List initialization
            ArrayList<Integer> list = new ArrayList<>();
            Integer integer_1 = 1;
            Integer integer_2 = 2;
            Integer integer_3 = 3;
            list.add(integer_1);
            list.add(integer_2);
            list.add(integer_3);
            System.out.println(list.size());
            list.remove(integer_2);
            System.out.println(list.size());
        }
    }
}
